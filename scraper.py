import contextlib
import os
import urllib.parse

import requests
from bs4 import BeautifulSoup

IMAGES_DIR = 'images'
BASE_URL = "https://pcsx2.net/"
WIKI_URL = 'https://wiki.pcsx2.net/'
COMPATIBILITY_URL = urllib.parse.urljoin(BASE_URL, '/compatibility-list.html')


class PCSX2Scraper:
    def __init__(self, url):
        self.url = url
        self.session = requests.session()

    @property
    def categories(self):
        resp = self.session.get(self.url)
        html = resp.text
        soup = BeautifulSoup(html, 'html.parser')
        index = soup.find('div', attrs={'id': 'Mod111'})
        ps = index.find_all('p')

        for p in ps:
            for a in p.find_all('a'):
                yield a.text, self._full_url(BASE_URL, a['href'])

    def games_list(self, category_url):
        resp = self.session.get(category_url)
        html = resp.text
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find('table', attrs={'id': 'rounded-corner'})

        for tbody in table.find_all('tbody'):
            yield self._parse_game(tbody)

    def download_image(self, wiki_url):
        resp = self.session.get(wiki_url)
        html = resp.text
        soup = BeautifulSoup(html, 'html.parser')

        if not soup.find('a', attrs={'class': 'image'}):
            return

        img_url = self._extract(soup.find('a', attrs={'class': 'image'}).find('img'), 'src')
        title = wiki_url.split('/')[-1]
        dest = os.path.join(IMAGES_DIR, '{}.jpg'.format(title))

        with contextlib.closing(self.session.get(self._full_url(WIKI_URL, img_url))) as res:
            with open(dest, 'wb') as f:
                for block in res.iter_content(1024):
                    if block:
                        f.write(block)

    @staticmethod
    def _full_url(base_url, relative_url):
        return urllib.parse.urljoin(base_url, relative_url)

    @staticmethod
    def _extract(tag, attr):
        return tag[attr] if tag else ''

    @classmethod
    def _parse_game(cls, row):
        tr1, tr2 = row.find_all('tr')

        tds1 = tr1.find_all('td')
        title = tds1[0].text
        serial = tds1[1].text
        img = cls._extract(tds1[2].find('img'), 'alt') if tds1[2] else ''
        status = cls._extract(tds1[3].find('hr'), 'title') if tds1[3] else ''
        version = tds1[4].text
        last_update = tds1[5].text

        tds2 = tr2.find_all('td')
        forums = cls._extract(tds2[2].find('a'), 'href') if tds2[2] else ''
        wiki = cls._extract(tds2[3].find('a'), 'href') if tds2[3] else ''

        return dict(title=title, serial=serial, image=img,
                    status=status, version=version, last_update=last_update,
                    forums=forums, wiki=wiki)


def main():
    scraper = PCSX2Scraper(COMPATIBILITY_URL)
    # Test
    for category, url in scraper.categories:
        print('*' * 10, category)
        if category >= 'B':
            for game in scraper.games_list(url):
                title = game.get('title')
                wiki_url = game.get('wiki')
                if wiki_url:
                    print('*' * 20, title)
                    scraper.download_image(wiki_url)


if __name__ == '__main__':
    main()
